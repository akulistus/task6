public class Item {
    private String name;
    private double price;
    private int rating;

    public Item(String name, double price, int rating) {
        this.name = name;
        this.price = price;
        this.rating = rating;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public int getRating() {
        return rating;
    }
}

public class Category {
    private String name;
    private Item[] items;

    public Category(String name, Item[] items) {
        this.name = name;
        this.items = items;
    }

    public String getName() {
        return name;
    }

    public Item[] getItems() {
        return items;
    }
}

public class Basket {
    private Item[] items;

    public Basket(Item[] items) {
        this.items = items;
    }

    public Item[] getItems() {
        return items;
    }
}

public class User {
    private String login;
    private String password;
    private Basket basket;

    public User(String login, String password, Basket basket) {
        this.login = login;
        this.password = password;
        this.basket = basket;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public Basket getBasket() {
        return basket;
    }
}

public class Main {
    public static void main(String[] args) {
        Item item1 = new Item("Телефон", 15000.0, 4);
        Item item2 = new Item("Наушники", 2500.0, 3);
        Item item3 = new Item("Ноутбук", 40000.0, 5);

        Item[] electronics = {item1, item2, item3};
        Category category1 = new Category("Электроника", electronics);

        Item item4 = new Item("Футболка", 1500.0, 4);
        Item item5 = new Item("Шорты", 2000.0, 3);

        Item[] clothes = {item4, item5};
        Category category2 = new Category("Одежда", clothes);

        Item[] basketItems = {item1, item4};
        Basket basket = new Basket(basketItems);

        User user = new User("user1", "password1", basket);
    }
}